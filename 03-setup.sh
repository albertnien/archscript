#!/bin/bash

cd ~ 

# get dotfile
git clone https://bitbucket.org/albertnien/dotfile.git 
mv dotfile/.* ${HOME}
# get vundle
git clone https://github.com/VundleVim/Vundle.vim.git ${HOME}/.vim/bundle/Vundle.vim


sudo chmod +x .config/bspwm/bspwmrc .config/bspwm/panel .config/bspwm/panel_bar .xinitrc .bashrc

# set PATH
echo "PANEL_FIFO=/tmp/panel-fifo" >> ${HOME}.bashrc
export PATH=$PATH:$HOME/.config/bspwm/ >> ${HOME}.bashrc

# install slim theme
sudo cp /home/albertnien/slim-default/* /usr/share/slim/themes/default/
rm -rf slim-default

###############
# install Aur #
###############
git clone https://aur.archlinux.org/broadcom-wl.git ~/b43/
git clone https://aur.archlinux.org/lemonbar-git.git ~/lemonbar/

# lemonbar
cd lemonbar
makepkg -s
sudo pacman -U *.pkg.tar.xz --noconfirm
cd ..
sudo rm -rf lemonbar

# b43
cd b43
makepkg -s
sudo pacman -U *.pkg.tar.xz --noconfirm
cd ..
sudo rm -rf b43

##########################
# install blackarch repo #
##########################
curl -O https://blackarch.org/strap.sh
sudo chmod +x strap.sh
sh strap.sh
rm strap.sh
