loadkeys dvorak

##################
# partition disk #
##################
#
lsblk
read -p "select disk :" device
cfdisk ${device}

# mkfs block
lsblk
read -p "select disk :" block
mkfs.ext4 ${block}

mount ${block} /mnt

######################
# start build system #
######################

# change mirrorlist
dhcpcd
pacstrap /mnt base base-devel 

##################
# genarate fstab #
##################
genfstab -p /mnt >> /mnt/etc/fstab

#####################
# bootloader config #
#####################
arch-chroot /mnt pacman -S grub --noconfirm
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt grub-install ${device}

# set keymap
arch-chroot echo "KEYMAP=dvorak" >> /etc/vconsole.conf

# set time zone
arch-chroot ln -sf /usr/share/zoneinfo/Asia/Taipei /etc/localtime

####################
# install packages #
####################
# normal use
arch-chroot pacman -S vim wget git alsa-utils alsa-utils curl --noconfirm
# desktop environment
arch-chroot pacman -S bspwm sxhkd slim xorg-server xorg-xinit feh chromium dmenu xterm --noconfirm
# wifi usage
arch-chroot pacman -S dialog wpa_supplicant --noconfirm

# start at boot
arch-chroot sudo systemctl enable slim.service  

arch-chroot cp 02-adduser.sh /mnt/root/
arch-chroot cp 03-setup.sh /mnt/root/
arch-chroot sudo chmod +x /mnt/root/02-adduser.sh /mnt/root/03-setup.sh
arch-chroot sudo sh /mnt/root/02-adduser.sh 